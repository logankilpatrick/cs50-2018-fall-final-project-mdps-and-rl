READEME:

My application was built and can be run using the CS50 IDE.  To run the website:
1). Cd project
2). Flask run
3). Click the link that is shown in the terminal.
4). Register with a new username and matching password.

Note: Some of the bones/infrastructure are reused from Pset8 in regards to the Login/Register/Error handling components.

Note: Much of the work involved in this project was theoretical.  I had to spend a considerable amount of time figuring out the best way to approach these problems such that other people would actually learn from this application.  I plan to transfer all of the work from this program onto a different website such that this application will go live for anyone to use.
