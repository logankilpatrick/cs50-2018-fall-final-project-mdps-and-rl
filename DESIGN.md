DESIGN: 

At the core of this website are many of the components from Pset8.  This idea of reusability was fundamental to my design such that I could focus on the content of this application.  

Each web page is constructed as follows: Course content laid out in simple paragraphs, a video or other form of multimedia, and a corresponding quiz. There is also a splash screen that gives the user feedback on whether or not that got the quiz question right. 

Logging in: When the user logs in, they will automatically be taken to the page they last completed the quiz on.  This will allow for easy ountinutation of the course material for students. Note: this implementation was designed directly from use of edX which does not do this, and it is a waste of the users time every session they login. 

Handling quiz retakes: The user is expected to complete the quizzes in sequential order.  However, not all users are the same and their curiosity may take them to the toolbar at the top.  Looking ahead at course material is not frowned upon but the user won't get credit for it unless they take all preceding quizzes. 


