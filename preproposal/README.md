# Preproposal

## What idea(s) do you have for your final project?

For the final project, I want to build a Machine Learning model such that two agents that share an alphabet are able
to learn to communicate about their enviorment.  This problem has some solutions out there, but I hope to extend
the functionality such that they can also learn to talk about the relationship between objects as well as present
this information in a graphical way (may create a website or just use standard plotting tools).

My project will be supported by Professor Amin at Harvard University and use the Python programming language and
the PyTorch machine learning framework.  I will also be using a virtual GPU to accelrate the speed of learning.

Resources:
[1] An Algorithm for Bootstrapping Communications, Jacob Beal, International Conference on Complex Systems (ICCS), June 2002.
A longer version appears as AI Memo 2001-016, August 2001.
Generating Communications Systems Through Shared Context, Jacob Beal, Master's Thesis, AI Tech Report 2002-002, January 2002.
[2] Lazaridou, Angeliki, et al. "Emergence of linguistic communication from referential games with symbolic and pixel input." arXiv preprint arXiv:1804.03984 (2018).


## If you plan to collaborate with one or two classmates, what are their names, and who are their section leaders?

N/A

## Do you have any questions of your own?

I plan to extend the functionality of already exisitng code such that the agents are able to talk about the spatial
relationships instead of just objects in a scene.
