import os
import datetime

from cs50 import SQL
from flask import Flask, flash, jsonify, redirect, render_template, request, session
from flask_session import Session
from tempfile import mkdtemp
from werkzeug.exceptions import default_exceptions, HTTPException, InternalServerError
from werkzeug.security import check_password_hash, generate_password_hash

from helpers import apology, login_required, lookup, usd

# Configure application
app = Flask(__name__)

# Ensure templates are auto-reloaded
app.config["TEMPLATES_AUTO_RELOAD"] = True

# Ensure responses aren't cached
@app.after_request
def after_request(response):
    response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    response.headers["Expires"] = 0
    response.headers["Pragma"] = "no-cache"
    return response

# Custom filter
app.jinja_env.filters["usd"] = usd

# Configure session to use filesystem (instead of signed cookies)
app.config["SESSION_FILE_DIR"] = mkdtemp()
app.config["SESSION_PERMANENT"] = False
app.config["SESSION_TYPE"] = "filesystem"
Session(app)

# Configure CS50 Library to use SQLite database
db = SQL("sqlite:///RL.db")


@app.route("/")
@login_required
def Home():
    holder = db.execute("SELECT CurrentLevel FROM users WHERE ID = :id", id = session["user_id"])
    num = db.execute("SELECT NumericalLevel FROM users WHERE ID = :id", id = session["user_id"])
    username1 = db.execute("SELECT username FROM users WHERE ID = :id", id = session["user_id"])

    if num == 5:
        return render_template(holder[0]["CurrentLevel"] + ".html", username = username1[0]["username"])
    else:
        return render_template(holder[0]["CurrentLevel"] + ".html")


@app.route("/Home", methods=["GET", "POST"])
@login_required
def goHome():
    print("In method")
    if request.method == "POST":
        answer = request.form.get("Answer")
        message = ""
        num = db.execute("SELECT NumericalLevel FROM users WHERE ID = :id", id = session["user_id"])
        if int(num[0]["NumericalLevel"]) > 1:
            message = "Nice job taking the quiz again! Your progress isn't beng saved for this since you already did it!"
        elif answer != "Agent, Reward, Action, Enviroment, Observation, Policy":
            message = "Good try.  That was not the rigth answer. Please review and try again."
        else:
            message = "Nice Job! You got it right.  Feel free to head to the next section by using the tool bar at the top. "
            db.execute("UPDATE users SET CurrentLevel = :value WHERE ID = :id", id = session["user_id"], value = "MDP")
            db.execute("UPDATE users SET NumericalLevel = :value WHERE ID = :id", id = session["user_id"], value = 2)


        return render_template("HomeQuiz.html", answer = answer, message = message )
    else:
        return render_template("Home.html")

@app.route("/MDP", methods=["GET", "POST"])
@login_required
def MDP():
    if request.method == "POST":
        answer = request.form.get("Answer")
        message = ""
        num = db.execute("SELECT NumericalLevel FROM users WHERE ID = :id", id = session["user_id"])
        if int(num[0]["NumericalLevel"]) != 2:
            message = "It looks like you are taking the quiz's out of order. Please make sure that the quizs are taken sequencially. "
        elif answer != "Trade off between Exploration and Exploitation":
            message = "Good try. You were close but did not get the right answer. Try reviewing and taking the quiz again by using the tool bar at the top.."

        else:
            message = "Nice Job! You got it right.  Feel free to head to the next section by using the tool bar at the top. "
            db.execute("UPDATE users SET CurrentLevel = :value WHERE ID = :id", id = session["user_id"], value = "Application")
            db.execute("UPDATE users SET NumericalLevel = :value WHERE ID = :id", id = session["user_id"], value = 3)

        return render_template("MDPQuiz.html", answer = answer, message = message )
    else:
        return render_template("MDP.html")



@app.route("/App", methods=["GET", "POST"])
@login_required
def App():
    if request.method == "POST":
        answer = request.form.get("Answer")
        message = ""
        num = db.execute("SELECT NumericalLevel FROM users WHERE ID = :id", id = session["user_id"])

        if int(num[0]["NumericalLevel"]) != 3:
            message = "It looks like you are taking the quiz's out of order. Please make sure that the quizs are taken sequencially. "

        elif answer != "We can move to (x+1,y) || (x+1,y+1) || (x,y+1).  The actions space is limited to make the problem simple.":
            message = "Good try. You were close but did not get the right answer. Try reviewing and taking the quiz again by using the tool bar at the top.."

        else:
            message = "Nice Job! You got it right.  Feel free to head to the next section by using the tool bar at the top. "
            db.execute("UPDATE users SET CurrentLevel = :value WHERE ID = :id", id = session["user_id"], value = "AdvancedApp")
            db.execute("UPDATE users SET NumericalLevel = :value WHERE ID = :id", id = session["user_id"], value = 4)


        return render_template("AppQuiz.html", answer = answer, message = message )
    else:
        return render_template("App.html")

@app.route("/AdvancedApp", methods=["GET", "POST"])
@login_required
def AdvancedApp():
    if request.method == "POST":
        answer = request.form.get("Answer")
        message = ""
        num = db.execute("SELECT NumericalLevel FROM users WHERE ID = :id", id = session["user_id"])
        if int(num[0]["NumericalLevel"]) != 4:
            message = "It looks like you are taking the quiz's out of order. Please make sure that the quizs are taken sequencially. "

        elif answer != "Limit the actions possible from each state, keep a small action space, and use a discrete action space.":
            message = "Good try. You were close but did not get the right answer. Try reviewing and taking the quiz again by using the tool bar at the top.."

        else:
            message = "Nice Job! You got it right.  That is the end of the course in its current form, but feel free to play around resources!"
            db.execute("UPDATE users SET CurrentLevel = :value WHERE ID = :id", id = session["user_id"], value = "Congrats")
            db.execute("UPDATE users SET NumericalLevel = :value WHERE ID = :id", id = session["user_id"], value = 5)

        username = db.execute("SELECT username FROM users WHERE ID = :id", id = session["user_id"])
        return render_template("Congrats.html", username = username[0]["username"] )
    else:
        return render_template("AdvancedApp.html")


@app.route("/About", methods=["GET", "POST"])
@login_required
def About():
    return render_template("About.html")

@app.route("/login", methods=["GET", "POST"])
def login():
    """Log user in"""

    # Forget any user_id
    session.clear()

    # User reached route via POST (as by submitting a form via POST)
    if request.method == "POST":

        # Ensure username was submitted
        if not request.form.get("username"):
            return apology("must provide username", 403)

        # Ensure password was submitted
        elif not request.form.get("password"):
            return apology("must provide password", 403)

        # Query database for username
        rows = db.execute("SELECT * FROM users WHERE username = :username",
                          username=request.form.get("username"))

        # Ensure username exists and password is correct
        if len(rows) != 1 or not check_password_hash(rows[0]["hash"], request.form.get("password")):
            return apology("invalid username and/or password", 403)

        # Remember which user has logged in
        session["user_id"] = rows[0]["id"]

        # Redirect user to home page
        return redirect("/")

    # User reached route via GET (as by clicking a link or via redirect)
    else:
        return render_template("login.html")


@app.route("/logout")
def logout():
    """Log user out"""

    # Forget any user_id
    session.clear()

    # Redirect user to login form
    return redirect("/")



@app.route("/register", methods=["GET", "POST"])
def register():

    if request.method == "POST":   #User input condition
        rows = db.execute("SELECT * FROM users WHERE username = :username", username=request.form.get("username"))
        if not request.form.get("username"):  # Ensure username was submitted
            return apology("must provide username", 400)

        elif  len(rows) > 0: #username already exists case
            return apology("username already exists", 400)

        elif not request.form.get("password"):
            return apology("must provide password", 400)

        elif request.form.get("password") != request.form.get("confirmation"):
            return apology("passwords do not match", 400)

        else: #Everything worked condition
            #print(check(request.form.get("username")))
            db.execute("INSERT INTO users (username, hash) VALUES(:username, :hash)",
            username = request.form.get("username"), hash = (generate_password_hash(request.form.get("password"))))
            rows = db.execute("SELECT * FROM users WHERE username = :username", username=request.form.get("username"))
            session["user_id"] = rows[len(rows) - 1]["id"]
            return render_template("Home.html")

    else:  # Initally loading the page condition.
        return render_template("register.html")


def errorhandler(e):
    """Handle error"""
    if not isinstance(e, HTTPException):
        e = InternalServerError()
    return apology(e.name, e.code)


# Listen for errors
for code in default_exceptions:
    app.errorhandler(code)(errorhandler)
