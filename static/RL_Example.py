# Welcome to the basic application of RL and MDP's.

stateHolder1 = [0.1, 0.2, 0.3, 0.4, 0.5] # each of these are lists of length 5
stateHolder2 = [0.2, 0.3, 0.4, 0.5, 0.6]
stateHolder3 = [0.6, 0.7, 0.1, 0.2, 0.3]
stateHolder4 = [0.7, 0.8, 0.9, 0.1, 0.1]
stateHolder5 = [0.3, 0.2, 0.8, 0.1, 0.7]

stateHolder = [stateHolder1, stateHolder2, stateHolder3, stateHolder4, stateHolder5]
#This lets us create a list of lists with the structure as shown above.

endFlag = False     #terminal flag to break out of the loop logic
x = 0               #x location
y = 0               #y location
i = 0               #counter
reward = 0.0        #total reward




#Search function which taks in the current x and y location, and gives back which of the 3 pixels to go to
def search(x, y, stateHolder):
    if x+1 < len(stateHolder) and y < len(stateHolder[0]):
        r1 = stateHolder[x+1][y]
    else: 
        r1 = 0.0
        
    if x+1 < len(stateHolder) and y+1 < len(stateHolder[0]):
        r2 = stateHolder[x+1][y+1]
    else: 
        r2 = 0.0
        
    if x < len(stateHolder) and y+1 < len(stateHolder[0]):
        r3 = stateHolder[x][y+1]    
    else: 
        r3 = 0.0
        
    if r1 == 0.0 and r2 == 0.0 and r3 == 0.0: #terminal case
        return (-1,-1)

    maxReward = max(r1, r2, r3)
    if r1 == maxReward:
        next_location = (x+1, y)
    elif r2 == maxReward:
        next_location = (x+1, y+1)
    else:
        next_location = (x, y+1)
    return next_location





while endFlag != True: #main logic loop! 
    if i == 0:
        reward += stateHolder[x][y] #[row, column]
        i += 1
    else:
        new_x, new_y = search(x, y, stateHolder)
        if new_x == -1 and new_y == -1:
            endFlag = True
            i += 1
            print("Terminal condition reached.")
        else:
            reward += stateHolder[new_x][new_y]
            x = new_x
            y = new_y
            i += 1
    print("Total Reward: ", reward, " as of step: ", i)
    print(x, ", ", y)
    print(stateHolder[x][y])