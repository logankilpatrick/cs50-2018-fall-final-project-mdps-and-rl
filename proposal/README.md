# Proposal

## What will (likely) be the title of your project?

Teaching rovers to communicate in the robotic future

## In just a sentence or two, summarize your project. (E.g., "A website that lets you buy and sell stocks.")

I hope to advance current research in the field of Machine Learning for teaching agents to communicate and also
explain ML to people in an interesting way. Some parts of this problem have already been solved, but they
are basic; I want to extend this such that the agents are able to communicate about more things.

## In a paragraph or more, detail your project. What will your software do? What features will it have? How will it be executed?

The project will be based off of existing code from https://github.com/nyu-dl/MultimodalGame
I hope to get that program to run as it was designed, and then extend it to allow for more communication vectors.
The goal will be to not only talk about the object that the agents both see, but the realtionship between the objects.
This has not been done before in any existing implimentation.  The website which will talk about all of my
research and findings will have a deep HTML, CSS, and python backend such that people can sign up to recive newsletter updates
about the research and more.  I also want there to be visual guides of many of the ideas I will be covering.

## If planning to combine CS50's final project with another course's final project, with which other course? And which aspect(s) of your proposed project would relate to CS50, and which aspect(s) would relate to the other course?

TODO, if applicable

## In the world of software, most everything takes longer to implement than you expect. And so it's not uncommon to accomplish less in a fixed amount of time than you hope.

### In a sentence (or list of features), define a GOOD outcome for your final project. I.e., what WILL you accomplish no matter what?

No matter what I plan to make a basic website which will explain how I approached the problem and explain machine
learning in this context(adaptive communcation between agents).

### In a sentence (or list of features), define a BETTER outcome for your final project. I.e., what do you THINK you can accomplish before the final project's deadline?

I think I can explain ML pretty well in this context, and also cover the landscape of research in the area of
ML + lingustics for agents to communicate.  This will set up the relevance of this research and make others interested in it.

### In a sentence (or list of features), define a BEST outcome for your final project. I.e., what do you HOPE to accomplish before the final project's deadline?

My hope is also to be able to advance the start of the art, but this could take longer than
the time allocated for this project. I want the rovers/agents to talk to one another about the spacial relationships between
objects.

## In a paragraph or more, outline your next steps. What new skills will you need to acquire? What topics will you need to research? If working with one of two classmates, who will do what?

I plan to look through the following resources to familarize myself with the state of the art:
[1] An Algorithm for Bootstrapping Communications, Jacob Beal, International Conference on Complex Systems (ICCS), June 2002.
A longer version appears as AI Memo 2001-016, August 2001.
Generating Communications Systems Through Shared Context, Jacob Beal, Master's Thesis, AI Tech Report 2002-002, January 2002.
[2] Lazaridou, Angeliki, et al. "Emergence of linguistic communication from referential games with symbolic and pixel input." arXiv preprint arXiv:1804.03984 (2018).
[3] https://github.com/nyu-dl/MultimodalGame.

I dont have much experince in machine learning, so this will be a very intensive introduction to it.
After that, I plan to try to get the  MultimodalGame code to run on my computer.  I will then create a basic web app
such that anyone can follow the steps I took to set up the problem and advance the state of the art. Following that,
I plan to try to understand and how to allow the agents to communicate about more than just objects.  I think that it
will not be too difficult to figure out a way for them to communicate about the spacial relationships between objects
in an enviorment.

I plan to propose a situation in the future where these type of systsm are important so that on another planet,
our overs can talk and collectivly achieve goals.
